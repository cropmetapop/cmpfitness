# cmpFitness

This tool is designed to compute the fitness of the genotypes found in
the populations in CropMetaPop simulations.

Currently, only the part where the optimums of populations are
different from one another is implemented (i.e. option `fitness_equal`
is not supported).

It works on the CropMetaPop multilocus genotypes output to compute,
for each genotype, the associated fitness given the optimum of the
population.

It returns almost the same file with an added column containing the
fitnesses of the genotypes.

## System requirements
- Python 3
- Pandas

## Required files
The programm will require at least 3 files in the simulation folder in
order to work:
- GenotypeMulti.csv
- settings.log
- fitness.csv

## Install the programm
You can run the programm without installing it by dropping the
`cmpfit.py` file anywhere you want, or by cloning this repository.

## Launching the programm
The programm can be run using the following command:
`$ python3 path/to/cmpfit/folder/cmpfit.py path/to/simulation/folder/
[number of cores]`

Note that the first path has to point to `cmpfit.py` while the second
one points to the folder of the simulation produced by CropMetaPop.

The argument `[number of cores]` allows the parallelisation of the
code and is **optionnal**. When left blank, it sets at the number
of cores available minus 1.

## Future developments
- Add some unit tests
