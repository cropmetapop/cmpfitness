import sys
import os
import time
import pandas as pd
import numpy as np

import multiprocessing

from joblib import Parallel, delayed


def get_simu_infos(data, nb_m_sel):
    NB_REPS = max(rawData["Replicate"]) + 1
    NB_POPS = max(rawData["Population"]) + 1
    NB_MARKS = int(max([i.split("Marker ")[1] for i in rawData.columns if "Marker" in i]))
    NB_GENS = int(rawData.columns[len(rawData.columns)-1].split(" ")[2]) +1

    return [NB_REPS, NB_POPS, NB_MARKS, int(nb_m_sel), NB_GENS]


def find_unique_genotypes(myPop):
    myGenos = myPop.groupby([i for i in myPop.columns if "Marker" in i]).size().reset_index().rename(columns={0:'val'})
    return(myGenos)


def extract_optima(PATH):
    with open(os.path.join(PATH, "setting.log")) as f:
        content = f.readlines()

    optiLst = []
    for line in content:
        if " Pop " in line:
            optiLst.append(float(line.split("|")[4].strip()))
        if "Markers" in line:
            break
    return(optiLst)


def associate_fitnesses(fitnessFichier, donnees, npop, optimum, nb_mk_sel):
    if len(fitnessFichier.columns) == 3:
        sys.exit("Pas encore implémenté")
    elif len(fitnessFichier.columns) == 4:
        fitnessFichier.columns = ["Pop", "Mk", "Geno", "Val"]
        myGenos = find_unique_genotypes(donnees)
        myGenos["val"] = myGenos["val"].astype('float64')
        colnames = myGenos.columns
        myNewGenos = []
        for nligne in range(0, len(myGenos)):
            val = []
            lignePart = []
            for nmk in range(0, nb_mk_sel):
                genActuel = myGenos.iloc[nligne].iloc[nmk]
                genActuelReverse = "/".join([genActuel.split("/")[1], genActuel.split("/")[0]])
                maVal = fitnessFichier[(fitnessFichier["Pop"] == npop) & (fitnessFichier["Mk"] == nmk) & ((fitnessFichier["Geno"] == genActuel) | (fitnessFichier["Geno"] == genActuelReverse) )]["Val"]

                if len(maVal) == 0:
                    sys.exit("Problème dans le fichier de fitness : aucune association avec le génotype trouvée.")

                val.append(float(maVal))
                lignePart.append(genActuel)
            [lignePart.append(i) for i in myGenos.iloc[nligne][len(lignePart):(len(myGenos.iloc[nligne])-1)]]
            val = np.mean(val)
            fit = np.exp(-0.5 * pow((val - optimum),2))
            maLigne = lignePart
            maLigne.append(str(fit))
            myNewGenos.append(maLigne)
        mNGen = pd.DataFrame(data=myNewGenos, columns = myGenos.columns)
    else:
        sys.exit("Problème dans le fichier de fitness")

    return mNGen


def write_res_fitness(tablResultat, myPath):
    tablResultat.to_csv(os.path.join(myPath, "resultatFitness.csv"), index=False)


start_time = time.time()

PATH = sys.argv[1]

try:
    nb_cores = int(sys.argv[2])
except:
    nb_cores = multiprocessing.cpu_count() -1


rawData = pd.read_csv(os.path.join(PATH, "GenotypeMulti.csv"))
fitnessFile = pd.read_csv(os.path.join(PATH, "fitness"), header=None)

if len(fitnessFile.columns) == 3:
    sys.exit("Pas encore implémenté")
elif len(fitnessFile.columns) == 4:
    fitnessFile.columns = ["Pop", "Mk", "Geno", "Val"]
else:
    sys.exit("Problème dans le fichier de fitness")
NB_MKSEL = max(fitnessFile["Mk"]) + 1
SIMUS_INFOS = get_simu_infos(rawData, NB_MKSEL)
NB_STEPS = SIMUS_INFOS[0] * SIMUS_INFOS[1] * SIMUS_INFOS[2] * SIMUS_INFOS[4]
LST_GENS = [" Gen " + str(i) for i in range(0, SIMUS_INFOS[4])]


def func_princ(PATH, rawData, fitnessFile, NB_MKSEL, npop):
    tableauSortie = []
    myPop = rawData[rawData["Population"] == npop]
    fits = associate_fitnesses(fitnessFile, myPop, npop, extract_optima(PATH)[npop], NB_MKSEL)
    colNonGen = [i for i in myPop.columns if "Gen" not in i]
    colGen = [i for i in myPop.columns if "Gen" in i]
    for nligne in range(0, len(myPop)):
        ligne = myPop.iloc[nligne]
        lToAdd = list(ligne[colNonGen])
        lToAdd.append(float(fits[fits.filter(like="Marker").eq(ligne.filter(like="Marker"), axis=1).all(axis=1)]["val"]))
        [lToAdd.append(int(i)) for i in ligne[colGen]]
        tableauSortie.append(lToAdd)
    return tableauSortie


nbLigne = 0
nbTotLigne = len(rawData)


class BatchCompletionCallBack(object):
    # Added code - start
    global total_n_jobs
    # Added code - end
    def __init__(self, dispatch_timestamp, batch_size, parallel):
        self.dispatch_timestamp = dispatch_timestamp
        self.batch_size = batch_size
        self.parallel = parallel

    def __call__(self, out):
        self.parallel.n_completed_tasks += self.batch_size
        this_batch_duration = time.time() - self.dispatch_timestamp

        self.parallel._backend.batch_completed(self.batch_size,
                                           this_batch_duration)
        self.parallel.print_progress()
        # Added code - start
        progress = self.parallel.n_completed_tasks / total_n_jobs
        print(
            "\rProgress: [{0:50s}] {1:.1f}%".format('#' * int(progress * 50), progress*100)
            , end="", flush=True)
        if self.parallel.n_completed_tasks == total_n_jobs:
            print('\n')
        # Added code - end
        if self.parallel._original_iterator is not None:
            self.parallel.dispatch_next()

import joblib.parallel
joblib.parallel.BatchCompletionCallBack = BatchCompletionCallBack

# from collections import defaultdict
# class CallBack(object):
#     completed = defaultdict(int)

#     def __init__(self, index, parallel):
#         self.index = index
#         self.parallel = parallel

#     def __call__(self, index):
#         CallBack.completed[self.parallel] += 1
#         print("done with {}".format(CallBack.completed[self.parallel]))
#         if self.parallel._original_iterable:
#             self.parallel.dispatch_next()


# import joblib.parallel
# joblib.parallel.CallBack = CallBack

total_n_jobs = SIMUS_INFOS[1]

res = Parallel(n_jobs=nb_cores)(delayed(func_princ)(PATH, rawData, fitnessFile, NB_MKSEL, npop) for npop in range(0, SIMUS_INFOS[1]))


colnames = [i for i in rawData.columns if "Gen" not in i]
colnames.append("Fit")
[colnames.append(i) for i in rawData.columns if "Gen" in i]

for npop, pop in enumerate(res):
    if npop == 0:
        myTableau = pd.DataFrame(data=pop, columns=colnames)
    else:
        myTableau = myTableau.append(pd.DataFrame(data=pop, columns=colnames), ignore_index=True)

write_res_fitness(myTableau, PATH)


stop_time = time.time()
print("Temps d'exécution : " + str(round(stop_time - start_time, 2)) + "s")
