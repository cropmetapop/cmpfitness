import os
import time

import pandas as pd
import numpy as np

nb_tests = 10

temps = []
for i in range(nb_tests):
    start_time = time.time()
    os.system("python3 cmpfit.py data/dataFit")
    stop_time = time.time()
    temps.append((stop_time - start_time))

print(np.mean(temps))
